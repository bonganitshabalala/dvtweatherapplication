package bongani.tshabalala.dvtweather.model;

/**
 * Created by Bongani on 2018-09-10.
 */

public class Wind {

    private String speed;

    private String deg;

    public Wind(String speed, String deg) {
        this.speed = speed;
        this.deg = deg;
    }

    public String getSpeed() {
        return speed;
    }

    public String getDeg() {
        return deg;
    }
}
