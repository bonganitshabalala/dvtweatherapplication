package bongani.tshabalala.dvtweather.model;

import java.util.List;

/**
 * Created by Bongani on 2018-09-10.
 */

public class FiveWeathers {
    private String dt_txt;

    private Main main;

    private List<WeatherResults> conditions;

    private Weather weather;

    public FiveWeathers(String dt_txt, Main main, List<WeatherResults> conditions, Weather weather) {
        this.dt_txt = dt_txt;
        this.main = main;
        this.conditions = conditions;
        this.weather = weather;
    }

    public String getDt_txt(){
        return dt_txt;
    }

    public Main getMain() {
        return main;
    }

    public List<WeatherResults> getConditions() {
        return conditions;
    }

    public Weather getWeather() {
        return weather;
    }
}
