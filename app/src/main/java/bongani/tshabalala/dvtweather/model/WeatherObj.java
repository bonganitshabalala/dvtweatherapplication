package bongani.tshabalala.dvtweather.model;

/**
 * Created by Bongani on 2018-09-11.
 */

public class WeatherObj {

    private String dayOfWeek;

    private String weatherIcon;

    private String weatherResult;

    private String weatherResultSmall;

    public WeatherObj(String dayOfWeek, String weatherIcon, String weatherResult, String weatherResultSmall) {
        this.dayOfWeek = dayOfWeek;
        this.weatherIcon = weatherIcon;
        this.weatherResult = weatherResult;
        this.weatherResultSmall = weatherResultSmall;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public String getWeatherIcon() {
        return weatherIcon;
    }

    public String getWeatherResult() {
        return weatherResult;
    }

    public String getWeatherResultSmall() {
        return weatherResultSmall;
    }
}
