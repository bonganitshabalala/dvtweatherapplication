package bongani.tshabalala.dvtweather.model;

/**
 * Created by Bongani on 2018-09-10.
 */

public class DateTimeEntry {

    public String date;
    public String mainHeadline;
    public String description;
    public String icon;

    public DateTimeEntry()
    {
    }
}
