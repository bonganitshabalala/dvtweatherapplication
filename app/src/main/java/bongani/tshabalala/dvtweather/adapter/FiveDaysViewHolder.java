package bongani.tshabalala.dvtweather.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bongani.tshabalala.dvtweather.R;

/**
 * Created by Bongani on 2018-09-11.
 */

public class FiveDaysViewHolder extends RecyclerView.ViewHolder{

    public TextView dayOfWeek;

    public ImageView weatherIcon;

    public TextView weatherTemp;


    public FiveDaysViewHolder(final View itemView) {
        super(itemView);
        dayOfWeek = (TextView)itemView.findViewById(R.id.day_of_week);
        weatherIcon = (ImageView)itemView.findViewById(R.id.weather_icon);
        weatherTemp = (TextView) itemView.findViewById(R.id.weather_temp);

    }
}
