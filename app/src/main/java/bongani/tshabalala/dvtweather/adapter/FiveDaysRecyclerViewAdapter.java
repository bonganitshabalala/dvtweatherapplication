package bongani.tshabalala.dvtweather.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import bongani.tshabalala.dvtweather.R;
import bongani.tshabalala.dvtweather.model.WeatherObj;

/**
 * Created by Bongani on 2018-09-11.
 */

public class FiveDaysRecyclerViewAdapter extends RecyclerView.Adapter<FiveDaysViewHolder> {

    private List<WeatherObj> dailyWeather;

    protected Context context;

    public FiveDaysRecyclerViewAdapter(Context context, List<WeatherObj> dailyWeather) {
        this.dailyWeather = dailyWeather;
        this.context = context;
    }

    @Override
    public FiveDaysViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FiveDaysViewHolder viewHolder = null;
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_description_layout, parent, false);
        viewHolder = new FiveDaysViewHolder(layoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FiveDaysViewHolder holder, int position) {

        holder.dayOfWeek.setText(dailyWeather.get(position).getDayOfWeek());

        // Including the weather image created from the icon - use glide for this
        Glide.with(context).load("http://openweathermap.org/img/w/" + dailyWeather.get(position).getWeatherIcon() + ".png")
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.weatherIcon);

        double mTemp = Double.parseDouble(dailyWeather.get(position).getWeatherResult());
        holder.weatherTemp.setText(String.valueOf(Math.round(mTemp)) + "°");

    }

    @Override
    public int getItemCount() {
        return dailyWeather.size();
    }

}
