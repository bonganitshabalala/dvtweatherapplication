package bongani.tshabalala.dvtweather;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import bongani.tshabalala.dvtweather.adapter.FiveDaysRecyclerViewAdapter;
import bongani.tshabalala.dvtweather.helper.Constants;
import bongani.tshabalala.dvtweather.helper.Util;
import bongani.tshabalala.dvtweather.model.DateTimeEntry;
import bongani.tshabalala.dvtweather.model.LocationObj;
import bongani.tshabalala.dvtweather.model.WeatherObj;
import im.delight.android.location.SimpleLocation;

public class MainActivity extends AppCompatActivity  {

    private SimpleLocation mLocation;
    private final int PERMISSION_REQUEST = 100;
    private FrameLayout mFrameLayour;
    private RecyclerView mRecycletView;
    private boolean mRequireFineGranularity = false;
    private boolean mPassiveMode = false;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private String apiCall;
    private LocationObj mLocationObj;
    private ImageView mOverlayImage;
    private TextView mTemperature;
    private TextView mTempMin;
    private TextView mTempCurrent;
    private TextView mTempMax;
  //  private SwipeRefreshLayout mSwipeRefreshLayout;
    LinearLayout linearLayout;
    Timer timer;
    TimerTask timerTask;
    final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create shortcut
        createShortCut();
        // Initialise components
        initUI();
        // Request location permission
        requestLocationPermission();


    }


    public void initUI() {

//        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.container);
//        mSwipeRefreshLayout.setOnRefreshListener(this);
//        mSwipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);

        mFrameLayour = (FrameLayout) findViewById(R.id.background);
        mRecycletView = (RecyclerView) findViewById(R.id.recyclerView);
        linearLayout = (LinearLayout) findViewById(R.id.ll);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        mRecycletView.setLayoutManager(gridLayoutManager);
        mRecycletView.setHasFixedSize(true);


    }

    public void createShortCut(){
        Intent homeShortcut = new Intent(getApplicationContext(), MainActivity.class);
        homeShortcut.setAction(Intent.ACTION_MAIN);
        homeShortcut.putExtra("duplicate", false);

        Intent removeShortcut = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
        removeShortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
        removeShortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, homeShortcut);
        sendBroadcast(removeShortcut);

        Intent installShortcut = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        installShortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
        Parcelable icon = Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.forest_sunny);
        installShortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);
        installShortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, homeShortcut);
        sendBroadcast(installShortcut);
    }


    private boolean requestLocationPermission() {
        // Permission has not been granted and must be requested.
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Permission Request")
                        .setMessage("Allow permission")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        PERMISSION_REQUEST);
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        mLocation =  new SimpleLocation(this, mRequireFineGranularity, mPassiveMode);

                        // if we can't access the location yet
                        if (!mLocation.hasLocationEnabled()) {
                            // ask the user to enable location access
                            SimpleLocation.openSettings(this);
                        }
                        latitude = mLocation.getLatitude();
                        longitude = mLocation.getLongitude();
                        apiCall = "http://api.openweathermap.org/data/2.5/weather?lat="+mLocation.getLatitude()+"&lon="+mLocation.getLongitude()+"&APPID="+ Constants.API_KEY+"&units=metric";
                        makeJsonObject(apiCall);
                       // Toast.makeText(this,"Lati "+ latitude + " loni "+ longitude,Toast.LENGTH_SHORT).show();
                    }

                } else {

                    // permission denied
                    // functionality that depends on this permission.
                    Toast.makeText(this,R.string.location_permission_denied,Toast.LENGTH_SHORT).show();
                    finish();

                }
                return;
            }

        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            mLocation =  new SimpleLocation(this, mRequireFineGranularity, mPassiveMode);

            // if we can't access the location yet
            if (!mLocation.hasLocationEnabled()) {
                // ask the user to enable location access
                SimpleLocation.openSettings(this);
            }
            latitude = mLocation.getLatitude();
            longitude = mLocation.getLongitude();
            apiCall = "http://api.openweathermap.org/data/2.5/weather?lat="+latitude+"&lon="+longitude+"&APPID="+ Constants.API_KEY+"&units=metric";
            makeJsonObject(apiCall);
           // Toast.makeText(this,"Lati "+ latitude + " loni "+ longitude,Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

        }
    }

    private void makeJsonObject(final String apiUrl){


        StringRequest stringRequest = new StringRequest(Request.Method.GET, apiUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("TAG", "Response " + response);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                mLocationObj = gson.fromJson(response, LocationObj.class);
                if (mLocationObj == null) {
                    latitude = -26.0299766;
                    longitude = 28.0096321;
                    apiCall = "http://api.openweathermap.org/data/2.5/weather?lat="+latitude+"&lon="+longitude+"&APPID="+ Constants.API_KEY+"&units=metric";
                    makeJsonObject(apiCall);
                } else {


                    View inflatedView = getLayoutInflater().inflate(R.layout.current_display, null);

                    String city = mLocationObj.getName() + ", " + mLocationObj.getSys().getCountry();
                    String todayDate = Util.getDateToday();
                    Long tempVal = Math.round(Math.floor(Double.parseDouble(mLocationObj.getMain().getTemp())));
                    Long tempMin = Math.round(Math.floor(Double.parseDouble(mLocationObj.getMain().getTemp_min())));
                    Long tempMax = Math.round(Math.floor(Double.parseDouble(mLocationObj.getMain().getTemp_max())));
                    String weatherTemp = String.valueOf(tempVal) + "°";
                    String weatherTempMin = String.valueOf(tempMin) + "°";
                    String weatherTempMax = String.valueOf(tempMax) + "°";
                    String weatherDescription = mLocationObj.getWeather().get(0).getDescription();
                    String windSpeed = mLocationObj.getWind().getSpeed();
                    String humidityValue = mLocationObj.getMain().getHumudity();
                    String weatherCondition = mLocationObj.getWeather().get(0).getMain();

                    ImageView mOverlayImage = (ImageView) inflatedView.findViewById(R.id.overlay);

                    switch (weatherCondition)
                    {
                        case "Clouds":
                            mOverlayImage.setBackground(getResources().getDrawable(R.drawable.forest_cloudy));
                            linearLayout.setBackgroundColor(getResources().getColor(R.color.cloudy));
                            break;
                        case "Rain":
                            mOverlayImage.setBackground(getResources().getDrawable(R.drawable.forest_rainy));
                            linearLayout.setBackgroundColor(getResources().getColor(R.color.rainy));
                            break;
                        case "Clear":
                            mOverlayImage.setBackground(getResources().getDrawable(R.drawable.forest_sunny));
                            linearLayout.setBackgroundColor(getResources().getColor(R.color.sunny));
                            break;
                    }
                    Log.d("TAG", "city " + city + " data " + todayDate+ " "+ weatherTemp + " " + weatherDescription);


                    TextView currentCity = (TextView) inflatedView.findViewById(R.id.textName);
                    currentCity.setText(city);
                    TextView currentCityDate = (TextView) inflatedView.findViewById(R.id.datetime);
                    currentCityDate.setText(todayDate);
                    TextView temp = (TextView) inflatedView.findViewById(R.id.temp);
                    temp.setText(weatherTemp);
                    TextView textTemp = (TextView) inflatedView.findViewById(R.id.tempCurrent);
                    textTemp.setText(weatherTemp+"\nCurrent");
                    TextView textTempMin = (TextView) inflatedView.findViewById(R.id.tempMin);
                    textTempMin.setText(weatherTempMin+"\nMin");
                    TextView textTempMax = (TextView) inflatedView.findViewById(R.id.tempMax);
                    textTempMax.setText(weatherTempMax+"\nMax");
                    TextView descrip = (TextView) inflatedView.findViewById(R.id.description);
                    descrip.setText(weatherCondition);

                    mFrameLayour.addView(inflatedView);

                    fiveDaysApiJsonObjectCall(mLocationObj.getName());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TAG", "Error " + error.getMessage());
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }

    private void fiveDaysApiJsonObjectCall(String city){
        String apiUrl = "http://api.openweathermap.org/data/2.5/forecast?q="+city+ "&APPID="+ Constants.API_KEY+"&units=metric";
        final List<WeatherObj> daysOfTheWeek = new ArrayList<WeatherObj>();
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, apiUrl,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                try {
                    ArrayList result = parseWeatherObject(response);
                    Log.d("TAG", "Response 5 days" + gson.toJson(result));

                    int[] everyday = new int[]{0,0,0,0,0,0,0};
                    JSONArray list = response.getJSONArray("list");
                    if(null != result){
                        for(int i = 0; i < result.size(); i++){
                          //  Log.d("TAG", "Response weatherInfo" + w);
                            JSONObject dtItem = list.getJSONObject(i);

                            String time = dtItem.getString("dt_txt");
                            String shortDay = Util.getDateTimr(time);

                            // Now go for the weather object
                            JSONArray weatherArray = dtItem.getJSONArray("weather");
                            JSONObject ob = (JSONObject) weatherArray.get(0);

                            String mainHeadline = ob.getString("main");
                            String description = ob.getString("description");
                            String icon = ob.getString("icon");

                            JSONObject weatherMain = dtItem.getJSONObject("main");

                            String temp = weatherMain.getString("temp");
                            String tempMin = weatherMain.getString("temp_min");
                            String tempMax = weatherMain.getString("temp_max");

                            Log.d("TAG", "Response Util.getDateTimr(time) " + Util.getDateTimr(time));
                            Log.d("TAG", "Response shortDay " + time);
                                if (Util.getDateTimr(time).equals("Monday") && everyday[0] < 1) {
                                    daysOfTheWeek.add(new WeatherObj(shortDay, icon, temp, tempMax));
                                    everyday[0] = 1;
                                }
                                if (Util.getDateTimr(time).equals("Tuesday") && everyday[1] < 1) {
                                    daysOfTheWeek.add(new WeatherObj(shortDay, icon, temp, tempMax));
                                    everyday[1] = 1;
                                }
                                if (Util.getDateTimr(time).equals("Wednesday") && everyday[2] < 1) {
                                    daysOfTheWeek.add(new WeatherObj(shortDay, icon, temp, tempMax));
                                    everyday[2] = 1;
                                }
                                if (Util.getDateTimr(time).equals("Thursday") && everyday[3] < 1) {
                                    daysOfTheWeek.add(new WeatherObj(shortDay, icon, temp, tempMax));
                                    everyday[3] = 1;
                                }
                                if (Util.getDateTimr(time).equals("Friday") && everyday[4] < 1) {
                                    daysOfTheWeek.add(new WeatherObj(shortDay, icon, temp, tempMax));
                                    everyday[4] = 1;
                                }
                                if (Util.getDateTimr(time).equals("Saturday") && everyday[5] < 1) {
                                    daysOfTheWeek.add(new WeatherObj(shortDay, icon, temp, tempMax));
                                    everyday[5] = 1;
                                }
                                if (Util.getDateTimr(time).equals("Sunday") && everyday[6] < 1) {
                                    daysOfTheWeek.add(new WeatherObj(shortDay, icon, temp, tempMax));
                                    everyday[6] = 1;
                                }
                                Log.d("TAG", "Response 5 daysOfTheWeek " + daysOfTheWeek);
                                FiveDaysRecyclerViewAdapter recyclerView = new FiveDaysRecyclerViewAdapter(MainActivity.this, daysOfTheWeek);
                                mRecycletView.setAdapter(recyclerView);
                           // }
                        }
                    }
              //  }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TAG", "Error " + error.getMessage());
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsObjRequest);
    }


    private ArrayList parseWeatherObject(JSONObject json) throws JSONException {
        ArrayList arrayList = new ArrayList();
        final List<WeatherObj> daysOfTheWeek = new ArrayList<WeatherObj>();
        int[] everyday = new int[]{0,0,0,0,0,0,0};
        //getting the list node from the json
        JSONArray list = json.getJSONArray("list");

        // Now iterate through each one creating our data structure and grabbing the info we need
        for(int i=0;i<list.length();i++)
        {
            // Create a new instance of our
            DateTimeEntry dtEntry = new DateTimeEntry();

            // Get the dateTime object
            JSONObject dtItem = list.getJSONObject(i);

            // pull out the date and put it in our own data
            dtEntry.date = dtItem.getString("dt_txt");

            // Now go for the weather object
            JSONArray weatherArray = dtItem.getJSONArray("weather");
            JSONObject ob = (JSONObject) weatherArray.get(0);

            // Grab what we are interested in
            dtEntry.mainHeadline = ob.getString("main");
            dtEntry.description = ob.getString("description");
            dtEntry.icon = ob.getString("icon");

            arrayList.add(dtEntry);
        }

        return arrayList;
    }

    public void changeCity(String city){
       // updateWeatherData(city);
    }

    public void startTimer() {
        //set a new Timer

        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 5000ms

        timer.schedule(timerTask, 5000, 5000); //

    }

    public void stoptimertask(View v) {

        //stop the timer, if it's not already null

        if (timer != null) {

            timer.cancel();

            timer = null;

        }

    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {

            public void run() {

                //use a handler to test location

                handler.post(new Runnable() {

                    public void run() {




                    }

                });

            }

        };

    }


}
