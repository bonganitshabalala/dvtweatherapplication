package bongani.tshabalala.dvtweather.helper;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Bongani on 2018-09-10.
 */

public class Util {


    public static String getDateToday()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("E, d MMMM");

        return dateFormat.format(calendar.getTime());
    }



    public static String getDateTimr(String time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String weekdays = "";
        try{

        //    Date date = dateFormat.parse(time);
            Date dt1=dateFormat.parse(time);
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(date);
            DateFormat format2=new SimpleDateFormat("EEEE");
            weekdays = format2.format(dt1);//calendar.getDisplayName(Calendar.DAY_OF_WEEK + 1, Calendar.SHORT, Locale.getDefault());
        }catch (ParseException e)
        {
            e.getMessage();
        }

        return weekdays;
    }

    public static byte[] getImage(String code) {
        HttpURLConnection con = null ;
        InputStream is = null;
        try {
            con = (HttpURLConnection) ( new URL(Constants.IMG_URL + code)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            // Let's read the response
            is = con.getInputStream();
            byte[] buffer = new byte[1024];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            while ( is.read(buffer) != -1)
                baos.write(buffer);

            return baos.toByteArray();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;

    }

}
